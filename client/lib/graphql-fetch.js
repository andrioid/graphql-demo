// Purpose: Simple wrapper to demonstrate the simplest possible way to talk to GraphQL servers
// Make sure that you enable CORS on the server (otherwise you will get TypeError Fetch Failed)
export default async (q) => {
    const body = JSON.stringify({query: q})
    const res = await fetch('http://localhost:3000/graphql', {
        method: 'POST',
        //mode: 'no-cors', // optional hack to bypass cors requirement of native fetch
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'content-type': 'application/json'            
        },
        body
    })
    const json = await res.json()
    return json.data
}
import React from 'react'

import { Form, Button } from 'semantic-ui-react'

import Menu from '../components/menu'

export default class NewPost extends React.Component {
    state = {
        title: '',
        body: '',
        saved: false,
        saving: false
    }

    handleSave(e, data) {
        e.preventDefault()
        console.log(e, data)
        alert('totally saving')
    }

    render () {
        return (
            <div style={{ padding: 20 }}>
                <h1>New Post</h1>
                <Menu />
                <Form onSubmit={(e, data) => this.handleSave(e, data)}>
                    <Form.Field>
                        <label>Title</label>
                        <input type="text" value={this.state.title} placeholder="Title"
                            onChange={(e, { value }) => this.setState({ title: value })}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Body</label>
                        <Form.TextArea value={this.state.body} onChange={(e, { value }) => this.setState({ body: value })} />
                    </Form.Field>
                    <Button type="submit">Save da fancy post man!</Button>
                </Form>
            </div>
        )
    }
}
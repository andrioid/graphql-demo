import React from 'react'
import 'isomorphic-fetch'

import Link from 'next/link'
import graphqlFetch from '../lib/graphql-fetch.js'
import { Button, List, Card } from 'semantic-ui-react'
import Menu from '../components/menu'
import PostPreview from '../components/post-preview'


const allBlogs = `{allBlogs {
  edges {
    node {
      id
      title
      authorByAuthorId {
        id
        name
      }
    }
  }
}}`

export default class IndexPage extends React.Component {

    static async getInitialProps ({ req }) {
        if (req) { 
            return {}
        }
        const json = await graphqlFetch(allBlogs)
        console.log(json)
        const posts = json.allBlogs.edges.map((post) => {
            return {
                ...post.node
            }
        })
        return {
            posts
        }
    }

    static defaultProps = {
        posts: []
    }

    render () {
        console.log('props', this.props)
        return (
            <div style={{ padding: 20 }}>
                <h1>GraphQL demo</h1>
                <Menu />
                <PostPreview posts={this.props.posts} />
            </div>
        )
    }
}
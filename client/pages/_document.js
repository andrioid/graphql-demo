// ./pages/_document.js
import Document, { Head, Main, NextScript } from 'next/document'
import flush from 'styled-jsx/server'

export default class MyDocument extends Document {
  static getInitialProps ({ req, renderPage }) {
    const page = renderPage()
    return { ...page }
  }

  render () {
    return (
     <html>
       <Head>
         <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/semantic.min.css"></link>
       </Head>
       <body>
         <Main />
         <NextScript />
       </body>
     </html>
    )
  }
}
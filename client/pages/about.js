import React from 'react'
import Link from 'next/link'

import Menu from '../components/menu'

export default class AboutPage extends React.Component {
    render () {
        return (
            <div style={{ padding: 20 }}>
                <h1>About</h1>
                <Menu />
                <Link href="/"><a>Back</a></Link>
            </div>
        )
    }
}
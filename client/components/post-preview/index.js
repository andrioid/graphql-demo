import React from 'react'
import { Card } from 'semantic-ui-react'

export default class PostPreview extends React.Component {
    render () {
        return (
            <Card.Group>
                {this.props.posts.map((post) => (
                    <Card key={post.id}>
                        <Card.Content>
                            <Card.Header content={post.title} />
                            <Card.Meta content={post.authorByAuthorId.name} />
                        </Card.Content>
                    </Card>
                ))}
            </Card.Group>            
        )
    }
}
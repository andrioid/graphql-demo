import React from 'react'
import { Menu } from 'semantic-ui-react'
import Link from 'next/link'

const MenuItem = ({href, title}) => (
    <Link href={href}>
        <Menu.Item
            name={href}
        >
            {title}
        </Menu.Item>
    </Link>
)

export default class PageMenu extends React.Component {
    render () {
        return (
            <Menu>
                <MenuItem href="/" title="Home" />
                <MenuItem href="/about" title="About" />
                <MenuItem href="/new" title="New post" />
            </Menu>
        )
    }
}
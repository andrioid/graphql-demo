# GraphQL demo

## Requirements

- `docker`
- `docker-compose`

## Development

### Launch
`docker-compose up` will start postgres, adminer (:8091), client (:8092) and server (:8090)
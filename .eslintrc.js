module.exports = {
	root: true,
	// https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style
	extends: 'standard',
	env: {
		'browser': true
	},
	// add your custom rules here
	'rules': {
		'no-tabs': 0,
		'indent': ['error', 'tab'],
		'no-mixed-spaces-and-tabs': ['error', 'smart-tabs'],
		// allow paren-less arrow functio<ns
		'arrow-parens': 0,
		// allow debugger during development
		'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
	}
}

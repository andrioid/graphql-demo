const postgraphql = require('postgraphql').postgraphql
const express = require('express')

const app = express()

// Enable CORS so browser and server can connect with fetch
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*')
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
	next()
})

app.use(postgraphql(
	'postgres://postgres:ohmydemo@postgres:5432',
	'public', // schema
	{
		'graphiql': true,
		'watchPg': true
	}
))
app.listen(3000)
